### Installation

MyTasks requires PHP >= 7.3 and composer installed.

First clone your project.

Go to the folder application using cd command on your cmd or terminal.

Then run:
```
$ composer install
```

Copy .env.example file to .env on the root folder.

Open your .env file and change the username (DB_USERNAME) and password (DB_PASSWORD) field correspond to your configuration.

Create a DB instance for the project
```sql
create database mytasks;
```
You can find the script DBinstance.sql in the main folder of the project.

Now run:
```
$ php artisan key:generate
$ php artisan migrate
```

If you want to seed the database, you can run:

```
$ php artisan db:seed
```

Now you can start the server:
```
$ php artisan serve
```

The application will be accessible to your local development environment. Open a web browser to http://localhost:8000/.

Thank you for taking the time to review this code.
Have a good day :)