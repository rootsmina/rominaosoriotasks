<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;

class TaskController extends Controller
{
    public function index()
    {
        $task = Task::orderBy('completed')->orderByDesc('created_at')->get();
        return $task->toJson();
    }

    public function store(Request $request)
    {
        
        $validatedData = $request->validate([
          'name' => 'required'
        ]);

        $task = new Task([
            'name' => $validatedData['name']
        ]);
        $task->save();

        return response()->json('Saved');
    }

    public function update($id, Request $request)
    {
        $task = Task::find($id);

        $request->validate([
            'name' => 'required'
        ]);
        $task->update($request->all());

        return response()->json('Updated');
    }

    public function destroy($id)
    {
        $task = Task::find($id);
        $task->delete();

        return response()->json('Deleted');
    }
}
