<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>My Tasks</title>
        <script src="{{ asset('js/app.js') }}" defer></script>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="container mt-5 px-lg-5">
            <h1 class="text-center my-3 pb-3"> My Tasks </h1>
            <div id="task-component"></div>
        </div>
    </body>
</html>