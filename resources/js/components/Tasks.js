import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

class TaskList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            items: [],
            name: '',
            errors: [],
            searchkey: '',
            filtered: [],
            modalWithoutAnimation: false,
            selectedTask:{},
            new_name: '',
        }
        this.delete=this.delete.bind(this);
        this.completed = this.completed.bind(this);
        this.add = this.add.bind(this);
        this.renderError = this.renderError.bind(this);
        this.filterTasks = this.filterTasks.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
    }

    componentDidMount() {
       this.getData();
    }

    handleNameChange = (e) => {
        this.setState({name: e.target.value});
    }

    handleEditNameChange = (e) => {
        this.setState({new_name: e.target.value});
    }

    handleSearchKeyChange = (e) => {
        this.setState({searchkey: e.target.value});
    }

    handleStatusChange = (e) => {
        let status = e.target.value;
        if (status !== '') {
            const filteredTasks = this.state.items.filter((task) => {
                return task.completed==status
            })
            this.setState({
              filtered: filteredTasks
            });
        } else{
            this.setState({
              filtered: this.state.items
            });
        }
    }

    getData(){

         axios.get('api/tasks').then(response => {
            this.setState({
                items: response.data,
                filtered: response.data
            })
        })

    }

    delete(id) {
        axios.delete('api/tasks/'+id);
        this.getData();
    }

    update(){
        axios.put('api/tasks/'+this.state.selectedTask.id, { name: this.state.new_name })
          .then(response => {
            this.getData();
            this.toggleModal();
          })
          .catch(error => {
            this.setState({
              errors: error.response.data.errors
            })
          })
    }

    completed = (item,e) => {
        let completed = event.target.checked;

        axios.put('api/tasks/'+item.id, { name: item.name,completed: completed })
          .then(response => {
            this.getData();
          })
          .catch(error => {
            this.setState({
              errors: error.response.data.errors
            })
          })
    }

    add(event) {
        event.preventDefault();
        const { name } = event.target
        axios.post('api/tasks/', { name: name.value })
          .then(response => {
            this.getData();
            this.setState({
              name: ''
            });
          })
          .catch(error => {
            this.setState({
              errors: error.response.data.errors
            })
          })
    }

    renderError (field) {
        if (!!this.state.errors[field]) {
          return (
            <span className='invalid-feedback d-block'>
              <strong>{this.state.errors[field][0]}</strong>
            </span>
          )
        }
      }

    filterTasks(){

        if (this.state.searchkey !== '') {
            const filteredTasks = this.state.items.filter((task) => {
                return task.name.toLowerCase().includes(this.state.searchkey.toLowerCase())
            })
            this.setState({
              filtered: filteredTasks
            });
        } else{
            this.setState({
              filtered: this.state.items
            });
        }

    }

    toggleModal(task) {
        //this.setState({ modalDialog: !this.state.modalDialog });
        this.setState({ 
            modalWithoutAnimation: !this.state.modalWithoutAnimation ,
            selectedTask: task? task : {},
            new_name: task?task.name:'',
            errors: []
        });
    }

    render() {
        const { filtered } = this.state

        return (
            <div className="container">
                <div className="row mb-2">
                    <div className="col-12">
                        <form onSubmit={this.add}>
                            <div className="input-group">
                                <input type="text" onChange={this.handleNameChange} value={this.state.name} id="name" name="name" placeholder="New Task" className="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm"/>
                                <div className="input-group-append">
                                    <button className="btn btn-success" type="submit">+</button>
                                </div>
                                {this.renderError('name')}
                            </div>
                        </form>
                    </div>
                </div>

                <div className="row mb-2">
                    <div className="col-9">
                        <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search" id="searchkey" name="searchkey" onChange={this.handleSearchKeyChange} value={this.state.searchkey} onKeyUp={this.filterTasks} />
                    </div>
                    <div className="col-3">
                        <select className="custom-select" onChange={this.handleStatusChange}>
                          <option value="">All Tasks</option>
                          <option value="1">Completed</option>
                          <option value="0">Uncompleted</option>
                        </select>
                    </div>
                </div>
              
                <div className="row">
                    <div className="col">
                        <table className="table mb-4">
                            <tbody>
                                {this.state.filtered.map((item,i)=>(
                                    <tr className={item.completed ? "alert alert-primary" : ""} key={ item.id }>
                                        <td className="w-75">
                                        <input className="mr-2" checked={ item.completed } name={item.id}  type="checkbox" onChange={() => this.completed(item)} />
                                            
                                                {item.completed ? <del>{item.name}</del> : item.name}
                                        </td>
                                        <td className="text-right">
                                            <button onClick={() => this.delete(item.id)} className="btn btn-danger btn-sm" type="button">Delete</button>
                                            <button onClick={() => this.toggleModal(item)} className="btn btn-warning btn-sm ml-1" type="button">Edit</button>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>

                <Modal isOpen={this.state.modalWithoutAnimation} fade={false} toggle={() => this.toggleModal()} >
                <ModalHeader toggle={() => this.toggleModal()}>Edit Task</ModalHeader>
                <ModalBody>
                    <input type="text" onChange={this.handleEditNameChange} value={this.state.new_name} id="new_name" name="new_name" className="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm"/>
                    {this.renderError('name')}
                </ModalBody>
                <ModalFooter>
                    <button className="btn btn-white" onClick={() => this.toggleModal()}>Close</button>
                    <button className="btn btn-white" onClick={() => this.update()}>Save</button>
                </ModalFooter>
            </Modal>

            </div>
  )}}

 

export default TaskList;

if (document.getElementById('task-component')) {
    ReactDOM.render(<TaskList />, document.getElementById('task-component'));
}
